[Reference video](https://www.youtube.com/watch?v=VqgTr-nd7Cg&t=2389s)

## project created using command

```bash
npx create-react-app todo_app
```

## vscode extensions

- Markdown emoji
- ES7+ React/Redux/React-Native snippet
- Prettier - Code formatter

## vscode How to enable native bracket matching:

```
settings.json

{
    "editor.bracketPairColorization.enabled": true,
    "editor.guides.bracketPairs":"active"
}
```

### firebase module

```bash
npm install firebase
```

### [material ui](https://mui.com/material-ui/getting-started/installation/)

```bash
#npm install material-ui/core
npm install @mui/material @emotion/react @emotion/styled
```
