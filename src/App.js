import { useState } from "react";
import Button from "@mui/material/Button";
import "./App.css";
import { FormControl, Input, InputLabel } from "@mui/material";
import Todo from "./Todo";

function App() {
  const [todos, setTodos] = useState([
    "Take dogs for a walk",
    "Take the rubbish out",
  ]);
  const [input, setInput] = useState("");
  console.log("🔫", input);
  const addTodo = (event) => {
    event.preventDefault(); //so page not reloads after form submit and state won't clear
    console.log("👽", "Im working, someone clicked addTodo button");
    setTodos([...todos, input]);
    setInput("");
  };
  return (
    <div className="App">
      <h1>Hello world 🚀🚀!</h1>
      <form>
        <FormControl>
          {/* https://mui.com/material-ui/api/form-control/ */}
          <InputLabel>✅ Write a Todo</InputLabel>
          <Input
            value={input}
            onChange={(event) => setInput(event.target.value)}
          />
        </FormControl>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          onClick={addTodo}
          disabled={!input}
        >
          Add Todo
        </Button>
        {/* button is jsx and Button is materialui based component https://mui.com/material-ui/react-button/ */}
      </form>

      <ul>
        {todos.map((todo) => (
          <Todo text={todo} />
        ))}
      </ul>
    </div>
  );
}

export default App;
