//rfce
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@mui/material";
import React from "react";

function Todo(props) {
  return (
    //<li>{props.text}</li>
    <List className="todo__list">
      <ListItem>
        <ListItemAvatar></ListItemAvatar>
        <ListItemText primary={props.text} secondary="Dummy deadline" />
      </ListItem>
    </List>
  );
}

export default Todo;
